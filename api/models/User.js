/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 let bcrypt = require('bcrypt-as-promised');
 let stripe = require('stripe')('sk_test_EzEtezZSFf2wSjXEdK7qKiyP');

module.exports = {

  attributes: {
  	email:{
  		type: 'string',
  		required: true,
  		unique: true
  	},
  	password:{
  		type: 'string',
  		required: true
  	},
    admin:{
      type: 'boolean',
      defaultsTo: false
    },
    customer_id:{
      type: 'string'
    }
  },
  createCustomer: function(opts){
    // opts = {user: null, token: ""}

    return new Promise(function(resolve,reject){

      stripe.customers.create({
        description: "Cliente: "+opts.user.email,
        source: opts.token
      },function(err,customer){
        if(err){
          console.log(err);
          reject(err);
        }else{
          User.update({id: opts.user.id},{customer_id: customer.id})
              .exec(resolve);
        }
      });

    })

    
  },
  beforeCreate:function(user,callback){

    let bcryptPromise = bcrypt.hash(user.password,10);

    let userCountPromise = User.count({});

    Promise.all([bcryptPromise,userCountPromise])
          .then(([hash,userCount])=>{
            user.password = hash;
            user.admin = userCount == 0;

            callback();
          });
  }
};

