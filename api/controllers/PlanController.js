module.exports = {
	find: function(req,res){
		Plan.find({}).then((docs)=>{
			res.view('plans/index',{plans: docs});
		}).catch(err=>{
			console.log(err);
			res.redirect('/');
		})
	},
	new: function(req,res){
		res.view('plans/new');
	},
	destroy:function(req,res){
		Plan.findOne({id:req.params.id}).then(plan=>{
			plan.destroy();
			res.redirect('/plan');
		})
	}
}